depy
====

`depy` is a dependency analysis tool for Python packages and modules,
as simple as possible.

`depy` works by looking for `import` and `from` statements inside files,
so it is somewhat limited to static dependencies.

`depy` can use Graphviz executables (through pydot) to generate graphs
in PNG (only, currently) through the `--draw` flag.

Use `depy --help` for usage instructions.
